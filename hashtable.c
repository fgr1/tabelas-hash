#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct No {
    char* chave;
    int value;
    struct No* next;
};

struct HashTable {
    struct No* table[97];
};

int string_hash(char * s) {
  char c;
  int p = 31, m = 97;
  int hash_value = 0, p_pow = 1;

  while (c = *s++) {
      hash_value = (hash_value + (c - 'a' + 1) * p_pow) % m;
      p_pow = (p_pow * p) % m;
  }
  return hash_value;
}

void init_hash_table(struct HashTable* ht) {
    for (int i = 0; i < 97; i++) {
        ht->table[i] = NULL;
    }
}

void inserir(struct HashTable* ht, char* chave, int value) {
    int index = string_hash(chave);
    struct No* new_No = (struct No*)malloc(sizeof(struct No));
    new_No->chave = chave;
    new_No->value = value;
    new_No->next = ht->table[index];
    ht->table[index] = new_No;
}

int busca(struct HashTable* ht, char* chave) {
    int index = string_hash(chave);
    struct No* current = ht->table[index];
    while (current != NULL) {
        if (strcmp(current->chave, chave) == 0) {
            return current->value;
        }
        current = current->next;
    }
    return -1;
}

void remove_chave(struct HashTable* ht, char* chave) {
    int index = string_hash(chave);
    struct No* current = ht->table[index];
    struct No* previous = NULL;
    while (current != NULL) {
        if (strcmp(current->chave, chave) == 0) {
            if (previous == NULL) {
                ht->table[index] = current->next;
            } else {
                previous->next = current->next;
            }
            free(current);
            return;
        }
        previous = current;
        current = current->next;
    }
}

void resposta_busca(struct HashTable* ht, char* chave){
    if(busca(ht, chave) == -1){
        printf("\nChave nao encontrada!");
    }
    else
        printf("\n%d", busca(ht, chave));
}

int main() {
    struct HashTable ht;
    init_hash_table(&ht);
    inserir(&ht, "Felipe", 10);
    inserir(&ht, "Gabriel", 20);
    inserir(&ht, "Reimer", 30);
    resposta_busca(&ht, "Felipe");
    resposta_busca(&ht, "Gabriel");
    remove_chave(&ht, "Felipe");
    resposta_busca(&ht, "Felipe");
}
